var gulp = require('gulp');
var server = require('karma').Server;
var browserSync = require('browser-sync');

var scriptsPath = [
  'lib/*.js',
  'test/*.js'
];

var resultsPath = [
  'results/*.html'
]

gulp.task('start', ['test', 'start:server', 'start:watch']);

gulp.task('start:server', function(){
  browserSync.init({
    server: './results',
  });
})

gulp.task('start:watch', function(){
  gulp.watch(scriptsPath, ['test']);
  gulp.watch(resultsPath, ['reload']);
});

gulp.task('test', function(done){
  new server({
    configFile: __dirname + '/karma.conf.js'    
  }, function(){
    done();
  }).start();
});

gulp.task('reload', browserSync.reload);