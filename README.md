# An Automatized Environment to apply coding dojos with javascript

## What is used

1. NodeJS
2. Gulp
3. Karma
4. Karma html reporter
5. Browser Sync
6. Jasmine
7. PhantomJS

## How to use

1. Clone the project
  ``` bash
  git clone git@bitbucket.org:darthaud/dojo-environment.git
  ```
2. Install dependencies
  ``` bash
  npm install
  ```

3. Start environment
  ``` bash
  npm start
  ```


